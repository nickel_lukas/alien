package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import controller.TargetController;
import model.Level;
import model.User;
import model.persistence.IPersistence;
import model.persistenceDummy.PersistenceDummy;
import view.game.GameGUI;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private AlienDefenceController controller;
	private List<Level> arrLevel;
	private JComboBox cboLevelChooser;
	private boolean showButton = true; 

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(AlienDefenceController controller, LevelController lvlControl, LeveldesignWindow leveldesignWindow, boolean showButton) {
		this.lvlControl = lvlControl;
		this.leveldesignWindow = leveldesignWindow;
		this.controller = controller;
		this.showButton = showButton;

		setLayout(new BorderLayout());

		JPanel pnlButtons = new JPanel();
		add(pnlButtons, BorderLayout.SOUTH);

		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);
		
		JButton btnPlay = new JButton("Spielen");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Thread t = new Thread("GameThread") {

					@Override
					public void run() {
					startGame();
					}
				};
				t.start();
				//MainMenue.btnSpielen_Clicked(alienDefenceController, arrLevel);
			}
		});
		if (showButton == true) {
		pnlButtons.add(btnPlay);
		}

		JButton btnScore = new JButton("Highscore");
		btnScore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Highscore(controller.getAttemptController(), arrLevel.get(cboLevelChooser.getSelectedIndex()));
			}
		});
		pnlButtons.add(btnScore);
		
		JButton btnExit = new JButton("Exit");
		btnScore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		pnlButtons.add(btnExit);
		
		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
	
	private String[] getLevelNames(List<Level> arrLevel) {
		String[] arrLevelNames = new String[arrLevel.size()];

		for (int i = 0; i < arrLevel.size(); i++) {
			arrLevelNames[i] = arrLevel.get(i).getName(); // Array aus Arraylist erstellt
		}

		return arrLevelNames;
	}
	
	public void startGame() {
		
		User user = new User(1, "test", "pass");
		
		System.out.println(this.tblLevels.getSelectedRow());
		
		int levelTemp = this.tblLevels.getSelectedRow();
		
		if (levelTemp < 0) {
			levelTemp = 0;
		}
		int level_id = Integer.parseInt((String) this.tblLevels.getModel().getValueAt(levelTemp, 0));
		
		
		Level level = controller.getLevelController().readLevel(level_id);
		
		GameController gameController = controller.startGame(level, user);
		new GameGUI(gameController).start();
	}
}
